from gengeo import *
import sys

xdim=float(sys.argv[1])
ydim=xdim*2.0
zdim=xdim

maxRadius=1.0
minPoint=Vector3(0.0,0.0,0.0)
maxPoint=Vector3(xdim,ydim,zdim)

box=BoxWithPlanes3D (
  minPoint=minPoint,
  maxPoint=maxPoint
)

box.addPlane(Plane(minPoint,Vector3(1.0,0.0,0.0)))
box.addPlane(Plane(minPoint,Vector3(0.0,1.0,0.0)))
box.addPlane(Plane(minPoint,Vector3(0.0,0.0,1.0)))
box.addPlane(Plane(minPoint,Vector3(-1.0,0.0,0.0)))
box.addPlane(Plane(minPoint,Vector3(0.0,-1.0,0.0)))
box.addPlane(Plane(minPoint,Vector3(0.0,0.0,-1.0)))

mntable=MNTable3D (
  minPoint=minPoint,
  maxPoint=maxPoint,
  gridSize=2.5*maxRadius,
  numGroups=1
)

packer=InsertGenerator3D(
  minRadius=0.2,
  maxRadius=maxRadius,
  insertFails=1000,
  maxIterations=1000,
  tolerance=1.0e-6,
  seed=1
)

packer.generatePacking (
  volume=box,
  ntable=mntable,
  groupID=0
)

mntable.generateBonds (
  groupID=0,
  tolerance=1.0e-5,
  bondID=0
)

mntable.write("box"+str(ydim)+".geo",1)
mntable.write("box"+str(ydim)+".vtu",2)
