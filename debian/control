Source: esys-particle
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Anton Gladky <gladk@debian.org>
Section: science
Testsuite: autopkgtest
Priority: extra
Build-Depends: autoconf,
               autoconf-archive,
               automake,
               debhelper (>= 10),
               doxygen,
               graphviz,
               libboost-all-dev,
               libcppunit-dev,
               libltdl7-dev,
               libtool,
               libvtk6-dev,
               mpi-default-bin,
               mpi-default-dev,
               python-demgengeo,
               texlive-latex-extra,
               texlive-fonts-recommended,
               texlive-latex-recommended,
               python-epydoc (>= 3.0.1+dfsg-6)
Standards-Version: 4.0.0
Vcs-Browser: https://anonscm.debian.org/cgit/debian-science/packages/esys-particle.git
Vcs-Git: https://anonscm.debian.org/git/debian-science/packages/esys-particle.git
Homepage: https://launchpad.net/esys-particle

Package: esys-particle
Architecture: any
Multi-Arch: foreign
Depends: libjs-jquery,
         mpi-default-bin,
         python,
         python-demgengeo,
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: paraview
Pre-Depends: ${misc:Pre-Depends}
Description: Software for particle-based numerical modelling. MPI version.
 ESyS-Particle is Open Source software for particle-based numerical modelling.
 The software implements the Discrete Element Method (DEM), a widely used
 technique for modelling processes involving large deformations, granular flow
 and/or fragmentation. ESyS-Particle is designed for execution on parallel
 supercomputers, clusters or multi-core PCs running a Linux-based operating
 system. The C++ simulation engine implements spatial domain decomposition via
 the Message Passing Interface (MPI). A Python wrapper API provides flexibility
 in the design of numerical models, specification of modelling parameters and
 contact logic, and analysis of simulation data. ESyS-Particle has been
 utilised to simulate earthquake nucleation, comminution in shear cells, silo
 flow, rock fragmentation, and fault gouge evolution, to name but a few
 applications.
